//це основний файл який створює всю сторінку.
// import * as flsFunctions from "./modules/functions.js";
//
// flsFunctions.isWebp();
import { Page } from "./modules/classes/Page.js";
import { allModals } from "./modules/variables.js";
import { clearPage, toogleExid } from "./modules/style/logOut.js";
import { closeFilter, showFilter } from "./modules/style/mainContent.js";
import { showLoginFormMob } from "./modules/style/loginForm.js";

// page містить в собі всі елементи сторінки
let page = new Page(allModals)

page.render().then(r => { }); // це метод класу Page він рендерить всі елементи на сторінку

toogleExid('.header__user-img', '.logOut')
clearPage()
showFilter()
closeFilter(".show__button", ".main__content-left")
closeFilter(".bg", ".main__content-left")
showLoginFormMob()