import { Modal } from "./Modal.js";
import { VisitCardiologist } from "../visit/VisitCardiologist.js";
import { VisitDentist } from "../visit/VisitDentist.js";
import { VisitTherapist } from "../visit/VisitTherapist.js";

export class ModalCreateVisit extends Modal {
	doctorSelect;

	openModalButton
	closeModalButton
	headerCloseModalButton
	modalBackground

	form
	formContent

	errorMessage

	constructor(params) {
		super(params);
		this.doctorSelect = document.getElementById("doctor__select")

		this.openModalButton = document.querySelector(".visit__button")
		this.closeModalButton = document.querySelector(".cancel")
		this.headerCloseModalButton = document.querySelector(".form__header-close")
		this.modalBackground = document.querySelector(".bg")

		this.form = document.querySelector('.form')
		this.formContent = document.querySelector('.form__content')

		this.errorMessage = document.querySelector('#error_message')
	}

	getDoctorInputs() {
		return document.querySelectorAll(".doctor__select")
	}

	bind() {
		super.bind();
		this.openModalButton.onclick = this.open.bind(this);//відкривається
		this.headerCloseModalButton.onclick = this.close.bind(this);
		this.closeModalButton.onclick = this.close.bind(this);
		this.modalBackground.onclick = this.close.bind(this);

		this.doctorSelect.onchange = this.chooseDoctor.bind(this);
	}

	open(e) {
		if (e !== undefined) {
			e.preventDefault()
		}

		this.doctorSelect.disabled = false;
		this.formContent.style.display = 'flex'
		this.form.style.transform = "translateY(0px)"
		this.modalBackground.style.display = 'flex'
		if (this.modalBackground) {
			document.body.style.overflow = 'hidden'
		}
	}

	close(e) {
		if (e !== undefined) {
			e.preventDefault()
		}

		Array.prototype.forEach.call(document.getElementsByClassName("form-input"), function (el) {
			el.value = ''
			if (el.tagName === "SELECT") {
				el.options[0].selected = true
			}
		});

		this.form.style.transform = "translateY(-1000px)"
		setTimeout(() => {
			if (this.domElement() !== undefined) {
				this.domElement().style.display = 'none'
			}
			this.modalBackground.style.display = 'none'
			document.body.style.overflow = 'scroll'
		}, 300)
	}

	chooseDoctor(e) {
		const selectedOption = e.target.value
		this.getDoctorInputs().forEach(element => {
			element.remove()
		});
		if (selectedOption === "Cardiologist") {
			this.addInput("pressure", "Звичайний тиск", "text")
			this.addInput("body-mass", "Індекс маси тіла", "text")
			this.addInput("hearth", "Перенесені захворювання", "text")
			this.addInput("age", "Вік", "text")
			document.body.style.overflowY = 'scroll'
		} else if (selectedOption === "Dentist") {
			document.body.style.overflowY = 'scroll'
			//Можна додати календар щоб вибарити дату
			this.addInput("date", "Дата останнього відвідування", "date")
		} else if (selectedOption === "Therapist") {
			this.addInput("age", "Вік", "text")
			document.body.style.overflowY = 'scroll'
		} else {
			document.body.style.overflowY = 'hidden'
		}
	}

	addInput(id, value, type) {
		const input = `
		 <div class="form__content-input doctor__select">
		 <input type='${type}' class="form-input" id="${id}" required=''>
		 <label for="${id}" class="label card-field">${value}</label>
		 <div class="underline"></div>
		 </div>`

		const parentElement = document.querySelector('.form__content-buttons')
		parentElement.insertAdjacentHTML("beforebegin", input)
	}

	createVisit(id) {
		const selectedDoctor = this.doctorSelect.options[this.doctorSelect.selectedIndex].value; //потрібно для того щоб діставати
		// конкретно те значення на яке натиснули

		let age

		const fullName = document.querySelector('.full__name').value
		if (fullName === '') {
			return "Не введено ПІБ"
		}
		const purpose = document.querySelector('#visit').value;
		if (purpose === '') {
			return "Не введено мету візиту"
		}
		const description = document.getElementById('description').value;
		if (description === '') {
			return "Не введено опис візиту"
		}
		const priority = document.querySelector('.priority-select').value;
		if (priority === '') {
			return "Не вибрано пріоритет візиту"
		}
		const status = 'open'
		if (document.getElementById('age') !== undefined && document.getElementById('age') !== null) {
			age = document.getElementById('age').value;
			if (age === '') {
				return "Не введено вік"
			}
		}
		switch (selectedDoctor) {
			case 'Cardiologist':
				const normalPressure = document.getElementById('pressure').value;
				if (normalPressure === '') {
					return "Не введено нормальний тиск"
				}
				const bodyMassIndex = document.getElementById('body-mass').value;
				if (bodyMassIndex === '') {
					return "Не введено індекс маси тіла"
				}
				const cardiovascularDiseases = document.getElementById('hearth').value;
				if (cardiovascularDiseases === '') {
					return "Не введені хвороби (Якщо нема - ввести 'Немає')"
				}
				this.close()
				return new VisitCardiologist(id, selectedDoctor, purpose, description, priority, fullName, status, normalPressure, bodyMassIndex, cardiovascularDiseases, age);
			case 'Dentist':
				const lastVisit = document.querySelector('#date').value;
				if (lastVisit === '') {
					return "Не введено дату останнього візиту"
				}
				this.close()
				return new VisitDentist(id, selectedDoctor, purpose, description, priority, fullName, status, lastVisit);
			case 'Therapist':
				this.close()
				return new VisitTherapist(id, selectedDoctor, purpose, description, priority, fullName, status, age);
			default:
				alert('Please select a doctor');
				return;
		}
	}
	addErrorMessage(errorMessage) {
		this.errorMessage.innerText = errorMessage
	}
	removeErrorMessage() {
		this.errorMessage.innerText = ''
	}
}
