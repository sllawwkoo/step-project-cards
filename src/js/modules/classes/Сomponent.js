//це файл з основним прототипом. Кожен елемент має певні однакові властивості.
// Клас Component наслідується іншими дочірніми класами.

export class Component {
    tagName;
    classNames;
    attributes;
    content;
    parentElement;

    constructor(params) {
        this.attributes = params.attributes;
        this.tagName = params.tagName;
        this.classNames = params.classNames;
        this.content = params.content;
        this.parentElement = params.parentElement;
    }

    //params це набір властивостей, який ми вказуємо коли вже створюємо об'єкт.

//звичайна функція по створенню DOM елемента на сторінці.
    createDomElement() {
        let element = document.createElement(this.tagName);

        if (this.classNames !== undefined) {
            element.classList.add(this.classNames);
        }
        if (this.content !== undefined) {
            element.innerHTML = this.content;
        }
        for (const key in this.attributes) {
            element.setAttribute(key, this.attributes[key]);
        }
        if (typeof this.parentElement === 'string') {
            let parent = document.getElementById(this.parentElement);
            parent.append(element);
        } else if (this.parentElement !== undefined) {
            this.parentElement.append(element);
        }
        return element;
    }

    domElement() {
        if( this.attributes !== undefined) {
            return document.getElementById(this.attributes.id)
        }
        return undefined
    }
}


