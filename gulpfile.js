//Основний модуль
import gulp from "gulp";
//Імпорт шляхів
import { path } from "./gulp/config/path.js";
//Імпорт спільних плагинів
import { plugins } from "./gulp/config/plugins.js"

//Передаємо значення в глобальну змінну 
global.app = {
	isBuild: process.argv.includes('--build'),
	isDev: !process.argv.includes('--build'),
	path: path,
	gulp: gulp,
	plugins: plugins
}


//Імпорт задач
import { reset } from "./gulp/tasks/reset.js";
import { html } from "./gulp/tasks/html.js";
import { server } from "./gulp/tasks/server.js";
import { scss } from "./gulp/tasks/scss.js";
import { js } from "./gulp/tasks/js.js";
import { images } from "./gulp/tasks/images.js";
import { fontsStyle, otfToTtf, ttfToWoff } from "./gulp/tasks/fonts.js";

//Спостерігач змін у файлах
function watcher() {
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.scss, scss);
    gulp.watch(path.watch.js, js);
    gulp.watch(path.watch.images, images);
}

// Обробка шрифтів
const fonts = gulp.series(otfToTtf, ttfToWoff, fontsStyle)

// Основні задачі
const mainTasks = gulp.series(fonts, gulp.parallel(html, scss, js, images));

//Побудова сценаріїв виконання задач
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server));
const build = gulp.series(reset, mainTasks);

// Єкспорт сценаріїв
export { dev }
export { build }

//Виконання сценарію за замовчуванням
gulp.task("default", dev);
